﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBanking
{
    class SavingAccount:BankAccount
    {
        public SavingAccount(double a, string b) : base(a, b)
        {

        }

        public override double CalculateInterest()
        {
            Console.WriteLine("Savings Calculate Interest");
            return this.Balance * 0.06;
        }

        public override void Withdraw(double a)
        {
            this.Balance = (this.Balance - a);
            Console.WriteLine("Savings withdraw");
            OverDraftFee();
            
        }
    }
}
