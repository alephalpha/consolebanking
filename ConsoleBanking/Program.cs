﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBanking
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount[] accounts = new BankAccount[2];

            accounts[0] = new CheckingAccount(12, "Jane");
            accounts[1] = new SavingAccount(5, "John");

            foreach (var curr in accounts)
            {
                Console.WriteLine(curr.Balance);
                curr.Withdraw(6);
                Console.WriteLine("{0}'s account has a balance of {1} and an interest of {2}", curr.Name,  curr.Balance, curr.CalculateInterest());
            }

            Console.ReadLine();
        }
    }
}
