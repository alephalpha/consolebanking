﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBanking
{
    abstract class BankAccount:OverDraft
    {
        private double balance;
        public double Balance
        {
            get
            {
                return balance;
            }
            set
            {
                balance = value;
            }
        }

        private string name;
        public String Name {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public BankAccount(double a, string b)
        {
            balance = a;
            name = b;
        }
        
        public abstract double CalculateInterest();

        public virtual void Withdraw(double a)
        {
            balance = (balance - a);
            OverDraftFee();   
        }

        public void OverDraftFee()
        {
            if(balance < 0)
            {
                balance = (balance - 5.00);
                Console.WriteLine("Bank Account Overdraft fee activated.");
            }
            
        }
    }
}
