﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBanking
{
    class CheckingAccount:BankAccount
    {
        public CheckingAccount(double a, string b) : base(a,b)
        {

        }

        public override double CalculateInterest()
        {
            Console.WriteLine("Checking Calculate Interest");
            return this.Balance * 0.13;
        }

        public override void Withdraw(double a)
        {
            this.Balance = (this.Balance - a);
            Console.WriteLine("Checking Withdraw");
            OverDraftFee();
            
        }


    }
}
